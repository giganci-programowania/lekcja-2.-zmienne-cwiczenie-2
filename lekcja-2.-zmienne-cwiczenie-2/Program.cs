﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lekcja_2._zmienne_cwiczenie_2
{
    class Program
    {
        static void Main(string[] args)
        {
            string tekst = "Ala ma kota";
            char znak = 'e';
            bool falsz = false;

            Console.WriteLine("Zmienna string: {0}", tekst);
            Console.WriteLine("Zmienna char: {0}", znak);
            Console.WriteLine("Zmienna bool: {0}", falsz);

            Console.ReadKey();
        }
    }
}
